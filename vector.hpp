#ifndef VECTOR_HPP_INCLUDED
#define VECTOR_HPP_INCLUDED
// Per size_t
#include <cstddef>
#include <cmath>
#include <iostream>
#include <stdexcept>

/*
    La classe Vector è un vettore in senso matematico.
    Ha una dimensione *fissata* a compile time (per cui è impossibile
    assegnare un vettore di una dimensione ad uno di un'altra) e definisce
    tutta una serie di operatori (somma, differenza, meno unario, prodotto per
    scalare, divisione per scalare) tali per cui è possibile usarlo in maniera
    "normale" in espressioni matematiche.
*/

// Un piccolo trucco di metaprogrammazione per fargli prendere automaticamente
// il tipo giusto per il prodotto per scalare.
// Di base usa come tipo per il prodotto per scalare il tipo di base...
template <typename TBaseType>
class VectorTraits
{
public:
    // Tipo su cui si basa
    typedef TBaseType TraitsBaseType;
    // Il tipo usato per i prodotti per scalare
    typedef TBaseType TraitsScalarType;
};

template<int N, typename TBaseType=double>
class Vector
 : public VectorTraits<TBaseType>
{
public:
    // La classe traits
    typedef VectorTraits<TBaseType> Traits;
    // Tipo usato per gli indici
    typedef std::size_t SizeType;
    // Lui stesso (per abbreviare)
    typedef Vector<N, TBaseType> ThisType;
    
    // "Importa" i typedef dei traits per comodità
    typedef typename Traits::TraitsBaseType BaseType;
    typedef typename Traits::TraitsScalarType ScalarType;
    
    typedef BaseType * iterator;
    typedef const BaseType * const_iterator;

    // Costruttore
    Vector()
    {
        for(SizeType i=0; i<N; ++i)
            v[i]=TBaseType();
    }

    // Operatore di accesso agli elementi
    BaseType & operator[](SizeType Index)
    {
        return v[Index];
    }
    
    // ... e relativa versione const
    const BaseType & operator[](SizeType Index) const
    {
        return v[Index];
    }

    BaseType & at(SizeType Index)
    {
        if(Index>=N)
            throw std::out_of_range("Index out of range.");
        return (*this)[Index];
    }
    
    const BaseType & at(SizeType Index) const
    {
        if(Index>=N)
            throw std::out_of_range("Index out of range.");
        return (*this)[Index];
    }
    /*
        Operatori matematici
        
        Tutti gli operatori in versione non-assegnamento usano la versione
        con assegnamento restituendo il relativo oggetto temporaneo
    */
    
    // Prodotto per scalare
    ThisType operator*(const ScalarType & Right) const
    {
        ThisType ret(*this);
        return ret*=Right;
    }
    
    ThisType & operator*=(const ScalarType & Right)
    {
        for(SizeType i=0; i<N; i++)
            (*this)[i]*=Right;
        return *this;
    }
    
    // Prodotto interno
    // Applicato a Vector<Vector > potrebbe dare risultati bizzarri
    ScalarType operator*(const ThisType & Right) const
    {
        ScalarType ret=0.;
        for(SizeType i=0; i<N; i++)
            ret+=(*this)[i]*Right[i];
        return ret;
    }
    
    // La norma deriva direttamente dalla definizione del prodotto interno
    ScalarType Norm() const
    {
        return std::sqrt((*this)*(*this));
    }
    
    // Distanza = norma della differenza tra i due punti
    ScalarType Distance(const ThisType & Right) const
    {
        return ((*this)-Right).Norm();
    }
    
    // Versore = vettore/norma
    ThisType UnitVector() const
    {
        return (*this)/(this->Norm());
    }
    
    // Divisione per scalare (se applicabile)
    ThisType operator/(const ScalarType & Right) const
    {
        ThisType ret(*this);
        return ret*(1./Right);
    }
    
    ThisType & operator/=(const ScalarType & Right)
    {
        (*this)*=(1./Right);
    }
    
    // Somma
    ThisType operator+(const ThisType & Right) const
    {
        ThisType ret(*this);
        return ret+=Right;
    }
    
    ThisType & operator+=(const ThisType & Right)
    {
        for(SizeType i=0; i<N; i++)
            (*this)[i]+=Right[i];
        return (*this);
    }
    
    // Differenza
    ThisType operator-(const ThisType & Right) const
    {
        ThisType ret(*this);
        return ret-=Right;
    }
    
    ThisType & operator-=(const ThisType & Right)
    {
        for(SizeType i=0; i<N; i++)
            (*this)[i]-=Right[i];
        return (*this);
    }
    
    // Meno unario (cambio di segno)
    ThisType operator-()
    {
        ThisType ret(*this);
        for(SizeType i=0; i<N; i++)
            ret[i]=-ret[i];
        return ret;
    }
    
    // Iteratori (per l'uso tramite STL)
    iterator begin()
    {
        return v;
    }
    
    const_iterator begin() const
    {
        return v;
    }
    
    iterator end()
    {
        return v+N;
    }
    
    const_iterator end() const
    {
        return v+N;
    }
    
    int size()
    {
        return N;
    }

protected:
    // Array interno
    BaseType v[N];
};

// Di nuovo il prodotto per scalare; necessario per far sì che si possa usare
// anche se lo scalare sta sulla sinistra dell'espressione
template<int N, typename TBaseType, typename TScalarType>
Vector<N, TBaseType> operator*(const TScalarType & Left, const Vector<N, TBaseType> & Right)
{
    // Di fatto richiama l'operatore già definito sopra
    return Right*Left;
}

// Specializzazione parziale nel caso in cui la classe base sia un Vector a
// sua volta (il tipo scalare per un Vector di Vector resta il tipo scalare
// dei suoi componenti)
template<int N, typename TVectorBaseType>
class VectorTraits< Vector<N, TVectorBaseType> >
{
public:
    // Tipo su cui si basa
    typedef Vector<N, TVectorBaseType> TraitsBaseType;
    // Il tipo usato per i prodotti per scalare
    typedef typename Vector<N, TVectorBaseType>::ScalarType TraitsScalarType;
};

// Scrive il vettore sullo stream di output specificato
template<int N, typename TBaseType>
std::ostream & operator<<(std::ostream & Os, Vector<N, TBaseType> Vec)
{
    typedef Vector<N, TBaseType> VT;
    Os<<"{";
    for(typename VT::SizeType i=0; i<N; i++)
    {
        Os<<Vec[i];
        if(i!=N-1)
            Os<<"; ";
    }
    Os<<"}";
    return Os;
}
#endif
