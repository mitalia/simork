#ifndef LRK_HPP
#define LRK_HPP
#include "matrix.hpp"
#include <iostream>
#include <stdexcept>
#include "gjs.hpp"

/**
 * Runge-Kutta solver for equations of type
 *
 * x' = A x
 *
 * (with x vector, A square matrix)
 */
template<int N, int S>
struct LRK
{
    typedef Matrix<S, S> rkT;       ///< type of the butcher matrix
    typedef Matrix<N, N> problemT;  ///< type of the problem matrix

    rkT rk;                         ///< butcher matrix
    Vector<S> butcher;              ///< butcher vector b
    problemT problem;               ///< problem matrix
    double tau;                     ///< integration step

    double t;                       ///< current timestamp
    Vector<N> x;                    ///< current solution


    typedef Matrix<S*N, S*N+1> baseT;   ///< type for the linear system matrix
    baseT base;                     ///< temporary linear system matrix (the first N columns are recycled for every step)
    
    /**
     * Constructs a Linear Runge Kutta equation solver instance
     * @param[in] rk        Butcher matrix to use
     * @param[in] butcher   Butcher vector b to use
     * @param[in] problem   Problem matrix
     * @param[in] tau       Integration step
     * @param[in] t0        Starting time
     * @param[in] x0        Initial condition
     */
    LRK(const rkT &rk, const Vector<S> &butcher, const problemT &problem, double tau, double t0, const Vector<N> &x0)
        : rk(rk), butcher(butcher), problem(problem), tau(tau), t(t0), x(x0)
    {
        // Check if method is applicable
        if(!problem.isUpTriangular())
        {
            std::cerr<<"Warning: cannot check if method is applicable to given problem";
        }
        else
        {
            for(int i=0; i<N; ++i)
            {
                if(problem(i,i)>0)
                    throw std::runtime_error("Equation is not stable");
            }
        }
        // Prepare the part of the linear system matrices that are the same for each step
        // to save some computations later
        for(int i=0; i<S; ++i)
        {
            for(int j=0; j<S; ++j)
            {
                problemT q = - tau * rk(i,j) * problem;
                base.putAt(q, i*N, j*N);
            }
        }
        base += baseT::identity();
    }

    /**
     * Do an integration step; leaves the new values in %x and %t
     */
    void step()
    {
        Vector<N> parz;
        // Copy the vector in the last column (replicated S times)
        for(int j=0; j<S*N; ++j)
            base(j, S*N) = x[j%N];
        Vector<S*N> gg;
        // Solve the linear system
        if(!gaussJordan(base, gg))
        {
            std::cout<<base;
            throw std::runtime_error("Singular system! =(");
        }

        for(int i=0; i<S; ++i)
        {
            Vector<N> gi;
            for(int j=0; j<N; ++j)
                gi[j]=gg[i*N+j];
            parz += butcher[i]*(problem*gi);
        }
        // RK step
        x += tau * parz;
        t += tau;
    }
};

#endif // LRK_HPP

