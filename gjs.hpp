#ifndef GJS_H
#define GJS_H
#include "matrix.hpp"

/**
 * Internal C-style Gauss-Jordan algorithm
 * @param[in]       n   rows
 * @param[in]       m   columns
 * @param[in,out]   mat working matrix
 * @return
 */
int gjs(int n, int m, double *mat);

/**
 * Performs a Gauss-Jordan elimination over the given matrix
 * to find a solution for the corresponding linear system
 * @param[in]  matrix    Input matrix
 * @param[out] res       Result vector
 * @return true if the algorithm succedeed, false in case of
 *         singular system (or other errors)
 */
template<int M>
bool gaussJordan(Matrix<M, M+1> matrix, Vector<M> &res)
{
    int ret = gjs(M, M+1, &matrix(0,0));
    for(int i=0; i<M; ++i)
        res[matrix(i, 0)] = matrix(i, M);
    return ret;
}

#endif
