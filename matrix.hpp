#ifndef MATRIX_HPP
#define MATRIX_HPP
#include "vector.hpp"
#include <iostream>
#include <algorithm>

/**
 * Class representing an M-rows, N-columns matrix
 */
template<int M, int N, typename BaseType=double>
class Matrix
{
public:
    BaseType m[M][N];       ///< internal storage

    /**
     * Number of rows
     */
    int rows()
    {
        return M;
    }

    /**
     * Number of columns
     */
    int cols()
    {
        return N;
    }

    /**
     * Default constructor - initializes an empty instance
     */
    Matrix()
    {
        for(int i=0; i<M; ++i)
        {
            for(int j=0; j<N; ++j)
                m[i][j]=BaseType();
        }
    }

    /**
     * Pastes an OxP matrix in the current instance with the given offsets
     * @param[in]   m2          Input matrix
     * @param[in]   rowOffset   Row offset
     * @param[in]   colOffset   Column offset
     */
    template<int O, int P>
    void putAt(const Matrix<O, P> &m2, int rowOffset, int colOffset)
    {
        // Copy
        for(int i=rowOffset, a=std::min(M, O+rowOffset); i<a; ++i)
        {
            for(int j=colOffset, b=std::min(N, P+colOffset); j<b; ++j)
                m[i][j]=m2(i-rowOffset,j-colOffset);
        }
    }

    /**
     * Access-to-elements operator (in C++ operator[] can take only one parameter...)
     * @param[in] i     Row index
     * @param[in] j     Column index
     */
    BaseType &operator()(size_t i, size_t j)
    {
        return m[i][j];
    }

    BaseType operator()(size_t i, size_t j) const
    {
        return m[i][j];
    }

    Matrix &operator+=(const Matrix &right)
    {
        for(int i=0; i<M; ++i)
        {
            for(int j=0; j<N; ++j)
                m[i][j] += right.m[i][j];
        }
        return *this;
    }

    Matrix operator+(const Matrix &right) const
    {
        Matrix ret(*this);
        ret += right;
        return ret;
    }

    Matrix &operator-=(const Matrix &right)
    {
        for(int i=0; i<M; ++i)
        {
            for(int j=0; j<N; ++j)
                m[i][j] -= right.m[i][j];
        }
        return *this;
    }

    Matrix operator-(const Matrix &right) const
    {
        Matrix ret(*this);
        ret -= right;
        return ret;
    }

    /**
     * Compound-assignment product with a scalar (on the right)
     */
    Matrix &operator*=(const BaseType &right)
    {
        for(int i=0; i<M; ++i)
        {
            for(int j=0; j<N; ++j)
                m[i][j] *= right;
        }
        return *this;
    }

    /**
     * Product with a scalar
     */
    Matrix operator*(const BaseType &right) const
    {
        Matrix ret(*this);
        ret *=right;
        return ret;
    }

    /**
     * Check if the matrix is an upper-triangular one
     * @return true if it is, false otherwise
     */
    bool isUpTriangular() const
    {
        if(M!=N)
            return false;
        for(int i=1; i<M; ++i)
        {
            for(int j=0; j<i; ++j)
            {
                if(m[i][j]!=0.)
                    return false;
            }
        }
        return true;
    }

    /**
     * Helper: returns an identity MxN matrix (if M!=N, it just fills up to the smallest of them)
     */
    static Matrix identity()
    {
        Matrix ret;
        for(int i=0; i<std::min(M, N); ++i)
            ret(i, i) = 1;
        return ret;
    }

};

/**
 * Product with a scalar (on the left)
 */
template<int M, int N, typename BaseType>
Matrix<M, N, BaseType> operator*(BaseType l, const Matrix<M, N, BaseType> &r)
{
    // Forward to the product on the right
    return r * l;
}

/**
 * Product on the left of an M vector with an MxN matrix
 */
template<int M, int N, typename BaseType>
Vector<N, BaseType> operator*(const Vector<M, BaseType> &l, const Matrix<M, N, BaseType> &r)
{
    Vector<N, BaseType> ret;
    for(int j=0; j<N; ++j)
    {
        BaseType acc=BaseType();
        for(int i=0; i<M; ++i)
            acc+=l[i]*r(i, j);
        ret[j]=acc;
    }
    return ret;
}

/**
 * Product on the right of an MxN matrix with an N vector
 */
template<int M, int N, typename BaseType>
Vector<M, BaseType> operator*(const Matrix<M, N, BaseType> &l, const Vector<N, BaseType> &r)
{
    Vector<M, BaseType> ret;
    for(int i=0; i<M; ++i)
    {
        BaseType acc=BaseType();
        for(int j=0; j<N; ++j)
            acc+=l(i,j)*r[j];
        ret[i]=acc;
    }
    return ret;
}

/**
 * Pretty-printer
 */
template<int M, int N, typename BaseType>
std::ostream & operator<<(std::ostream &os, const Matrix<M, N, BaseType> &r)
{
    os<<"[\n";
    for(int i=0; i<M; ++i)
    {
        os<<" [";
        for(int j=0; j<N; ++j)
        {
            os<<r(i,j);
            if(j!=N-1)
                os<<", ";
        }
        os<<"]\n";
    }
    os<<"]\n";
    return os;
}

#endif // MATRIX_HPP
