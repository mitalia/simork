#include <iostream>
#include "matrix.hpp"
#include "lrk.hpp"
#include <math.h>
#include <stdio.h>

Vector<3> smallSol(double t)
{
    Vector<3> ret;
    ret[0] = exp(-t) * (17./2. - 12.*exp(-t) + 9./2.*exp(-2.*t));
    ret[1] = 4.*exp(-2.*t) - 3.*exp(-3.*t);
    ret[2] = exp(-3. * t);
    return ret;
}

void printError(const char *str, Vector<3> err)
{
    printf("L'errore %s vale (", str);
    for(int i=0; i<3; ++i)
    {
        printf("%.16g", fabs(err[i]));
        if(i!=2) printf(", ");
    }
    puts(")");
}

void die(const std::string &reason)
{
    std::cerr<<reason;
    std::exit(1);
}

template<int S>
void smallSystem(double t1, double tau, Matrix<S,S> rk, Vector<S> butcher, const char *fname, const char *method)
{
    printf("=== Sistema test - integrazione su [%g, %g] con passo %g e metodo %s ===\n", 0., t1, tau, method);
    FILE *output = fopen(fname, "w");
    if(!output)
        die(std::string("Cannot open ") + fname);
    Matrix<3, 3> p;
    p(0,0) = -1.;       p(0,1) =  3.;
                        p(1,1) = -2.;       p(1,2) =  3.;
                                            p(2,2) = -3.;

    Vector<3> x0;
    x0[0] = x0[1] = x0[2] = 1.;

    LRK<3, S> sol(rk, butcher, p, tau, 0., x0);
    Vector<3> exact;

    Vector<3> maxErrTime;
    Vector<3> maxError;

    for(int count=0 ; sol.t<=t1; count++)
    {
        exact = smallSol(sol.t);
        fprintf(output, "%.16f\t", sol.t);
        for(int i=0; i<3; ++i)
            fprintf(output, "%.16g\t", sol.x[i]);
        for(int i=0; i<3; ++i)
            fprintf(output, "%.16g\t", exact[i]);
        for(int i=0; i<3; ++i)
        {
            double err = fabs(sol.x[i] - exact[i]);
            if(err>maxError[i])
            {
                maxError[i] = err;
                maxErrTime[i] = sol.t;
            }
        }
        putc('\n', output);
        if(count==1)
            printError("iniziale", sol.x-exact);
        sol.step();
    }
    printError("finale", sol.x-exact);

    puts("Errori massimi: ");
    for(int i=0; i<3; ++i)
        printf("err[%d] = %.16g @ t=%.7f\n", i, maxError[i], maxErrTime[i]);
    puts("");
}

template<int S>
void test_L_Stability(double tau, Matrix<S,S> rk, Vector<S> butcher, const char *method)
{
    printf("=== Test per la L-stabilità del metodo - integrazione su [%g, %g] con passo %g e metodo %s ===\n", 0., tau, tau, method);
    Matrix<3, 3> p;
    p(0,0) = -1.;       p(0,1) =  3.;
                        p(1,1) = -2.;       p(1,2) =  3.;
                                            p(2,2) = -3.;

    Vector<3> x0;
    x0[0] = x0[1] = x0[2] = 1.;

    LRK<3, S> sol(rk, butcher, p, tau, 0., x0);
    sol.step();
    printf("Con un passo di integrazione pari all'ampiezza dell'intervallo la soluzione vale (");
    for(int i=0; i<3; ++i)
    {
        printf("%.16g", sol.x[i]);
        if(i!=2) printf(", ");
    }
    puts(")");
}

Vector<3> bigPartialSol(double t)
{
    Vector<3> ret;
    // 18
    ret[0] = exp(-18. * t) * (1. + 420. * (1 - exp(-t)) - 200.*(1. - exp(-2.*t)));
    // 19
    ret[1] = exp(-19. * t) * (21. - 20. * exp(-t));
    // 20
    ret[2] = exp(-20. * t);
    return ret;
}

template<int S>
void bigSystem(double t1, double tau, Matrix<S,S> rk, Vector<S> butcher, const char *fname, const char *method)
{
    printf("=== Problema assegnato - integrazione su [%g, %g] con passo %g e metodo %s ===\n", 0., t1, tau, method);
    FILE *output = fopen(fname, "w");
    if(!output)
        die(std::string("Cannot open ") + fname);

    Matrix<20,20> p;
    for(int i=0; i<20; ++i)
        p(i,i) = -(i+1);
    for(int i=1; i<20; ++i)
        p(i-1,i) = 20;
    Vector<20> x0;
    for(int i=0; i<20; ++i) x0[i]=1.;

    Vector<3> exact;

    Vector<3> partialApprox;
    Vector<3> maxErrTime;
    Vector<3> maxError;

    LRK<20, S> sol(rk, butcher, p, tau, 0., x0);
    for(int count=0 ; sol.t<=t1; count++)
    {
        exact = bigPartialSol(sol.t);
        fprintf(output, "%.16f\t", sol.t);
        for(int i=0; i<20; ++i)
            fprintf(output, "%.16g\t", sol.x[i]);
        for(int i=0; i<3; ++i)
            fprintf(output, "%.16g\t", exact[i]);
        fprintf(output, "\n");
        for(int i=17; i<20; ++i)
            partialApprox[i-17] = sol.x[i];
        for(int i=0; i<3; ++i)
        {
            double err = fabs(partialApprox[i] - exact[i]);
            if(err>maxError[i])
            {
                maxError[i] = err;
                maxErrTime[i] = sol.t;
            }
        }
        if(count==1)
            printError("iniziale", partialApprox-exact);
        sol.step();
    }
    printError("finale", partialApprox-exact);

    puts("Errori massimi: ");
    for(int i=0; i<3; ++i)
        printf("err[%d] = %.16g @ t=%.7f\n", i, maxError[i], maxErrTime[i]);
    puts("");
}

int main()
{
    Matrix<2, 2> d_imp;
    d_imp(0,0) = (3.+sqrt(3.))/6.;      d_imp(0,1) = 0;
    d_imp(1,0) = -sqrt(3.)/3.;          d_imp(1,1) = (3.+sqrt(3.))/6.;
    Vector<2> d_imp_butcher;
    Matrix<3, 3> radau;
    radau(0,0) = (88.-7.*sqrt(6.))/360.;		radau(0,1) = (296.-169.*sqrt(6))/1800.;			radau(0,2) = (-2.+3.*sqrt(6.))/225.;
    radau(1,0) = (296.+169.*sqrt(6.))/1800.;	radau(1,1) = (88.+7.*sqrt(6.))/360.;			radau(1,2) = (-2.-3.*sqrt(6.))/225.;
    radau(2,0) = (16.-sqrt(6.))/36.;			radau(2,1) = (16.+sqrt(6.))/36.;				radau(2,2) = 1./9.;
    Vector<3> radau_butcher;
    radau_butcher[0] = (16.-sqrt(6.))/36.;		radau_butcher[1] = (16.+sqrt(6.))/36.;			radau_butcher[2] = 1./9.;
    Matrix<2, 2> radau3;
    radau3(0,0) = 5./12.;		radau3(0,1) = -1./12.;
    radau3(1,0) = 3./4.;		radau3(1,1) = 1./4.;
    Vector<2> radau3_butcher;
    radau3_butcher[0] = 3./4.;	radau3_butcher[1] = 1./4.;
    d_imp_butcher[0] = d_imp_butcher[1] = 1./2.;
    smallSystem(10., 1E-1, d_imp, d_imp_butcher, "small-1E-1.tsv", "diagonalmente implicito");
    smallSystem(10., 1E-2, d_imp, d_imp_butcher, "small-1E-2.tsv", "diagonalmente implicito");
    smallSystem(10., 1E-3, d_imp, d_imp_butcher, "small-1E-3.tsv", "diagonalmente implicito");
    test_L_Stability(100, d_imp, d_imp_butcher, "diagonalmente implicito");
    bigSystem(10., 1E-1, d_imp, d_imp_butcher, "big_0-10_1E-1.tsv", "diagonalmente implicito");
    bigSystem(10., 1E-2, d_imp, d_imp_butcher, "big_0-10_1E-2.tsv", "diagonalmente implicito");
    bigSystem(10., 1E-3, d_imp, d_imp_butcher, "big_0-10_1E-3.tsv", "diagonalmente implicito");
    bigSystem(10000., 1E-1, d_imp, d_imp_butcher, "big_0-10000_1E-1.tsv", "diagonalmente implicito");
    bigSystem(10000., 1E-2, d_imp, d_imp_butcher, "big_0-10000_1E-2.tsv", "diagonalmente implicito");
    bigSystem(10000., 1E-3, d_imp, d_imp_butcher, "big_0-10000_1E-3.tsv", "diagonalmente implicito");

    return 0;
}


//Here we just verified that Gauss-Jordan works.
void gjtest()
{
    Matrix<2, 3> a;
    std::cout<<"Matrix:\n";
    for(int i=0; i<a.rows(); ++i)
    {
        for(int j=0; j<a.cols(); ++j)
            std::cin>>a(i, j);
    }
    Vector<2> v;
    std::cout<<a;
    std::cout<<gaussJordan(a, v)<<"\n";
    std::cout<<v;
}

//Here we just verified that the class Matrix works.
void matrixtest()
{
    std::cout<<"hello world!\n";
    Matrix<3, 2> a;
    std::cout<<"Matrix:\n";
    for(int i=0; i<a.rows(); ++i)
    {
        for(int j=0; j<a.cols(); ++j)
            std::cin>>a(i, j);
    }
    std::cout<<a;
    std::cout<<"Vector:\n";
    Vector<2> b;
    for(int i=0; i<b.size(); ++i)
        std::cin>>b[i];
    std::cout<<b;
    Vector<3> c=a*b;
    std::cout<<c;
}
