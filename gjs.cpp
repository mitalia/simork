#include <stdlib.h>
#include <math.h>
#include "gjs.hpp"


// O(r) + O(r*(r*r + c + r*c)) + O(r*r) + O(r) => O(r^2 (r + c)) = O(r^3) + O(r^2 c) = O(r (Nq + N))

int gjs(int n,
        int m,
        double *mat)
{
    double **M;
    int r;
    M = (double **)malloc(n*sizeof(double *));
    if (M == NULL)
        return -1;
    // O(righe)
    for (r=0; r<n; r++)
        M[r] = mat + r*m;
    // O(righe)
    for (r=0; r<n; r++)
    {
        /* Pivoting scalato per stabilita' numerica */
        {
            /* Trova la riga pivot */
            int max_row = r;
            double max_absv = fabs( M[r][r] );
            {
                int r1, c1;
                // ~righe/2 => O(righe)
                for (r1=r+1; r1<n; r1++)
                {
                    double v = fabs( M[r1][r] );
                    double m = v;
                    // O(righe)
                    for (c1=r+1; c1<n; c1++)
                    {
                        double x = fabs( M[r1][c1] );
                        if (x > m) m = x;
                    }
                    if (m)
                        v = v / m;
                    if ( v > max_absv )
                    {
                        max_row = r1;
                        max_absv = v;
                    }
                }
            }

            /* Test per sistema singolare */
            if (max_absv < 1E-10)
            {
                free(M);
                return 0;
            }

            /* Eventualmente la scambia la riga pivot con la corrente */
            if (max_row != r)
            {
                double *tr = M[r];
                M[r] = M[max_row];
                M[max_row] = tr;
            }
        }

        /* Normalizzazione riga */
        // O(col)
        {
            int c;
            double k = 1.0 / M[r][r];
            M[r][r] = 1.0;
            for (c=r+1; c<m; c++)
                M[r][c] *= k;
        }

        /* Annullamento colonna sottostante */
        {
            int r1;
            // O(righe)
            for (r1 = r+1; r1<n; r1++)
            {
                int c;
                double k = M[r1][r];
                M[r1][r] = 0.0;
                // O(colonne)
                for (c=r+1; c<m; c++)
                {
                    M[r1][c] -= k*M[r][c];
                }
            }
        }
    }

    /* Risalita */
    {
        int c;
        // O(righe)
        for (c=n-1; c>0; c--)
        {
            int r;
            // O(righe)
            for (r=0; r<c; r++)
            {
                int c1;
                double k = M[r][c];
                M[r][c] = 0.0;
                for (c1=r+1; c1<m; c1++)
                {
                    M[r][c1] -= k*M[c][c1];
                }
            }
        }
    }

    /* Copia in prima colonna la permutazione delle righe */
    // O(righe)
    for (r=0; r<n; r++)
    {
        mat[r*m] = (M[r]-mat)/m;
    }
    free(M);
    return 1;
}
